package com.example.placeorder;

import org.hamcrest.core.IsNull;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.internal.matchers.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.not;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
class PlaceorderApplicationTests {

	@Autowired
	TestRestTemplate restTemplate;
	@LocalServerPort
	int randomServerPort;

	ShoppingCartService shoppingCartService;

	URI uri;

	@BeforeEach
	void setUp() throws URISyntaxException {
		final String baseUrl = "http://localhost:"+randomServerPort+"/createOrderFromShoppingCart/";
		uri = new URI(baseUrl);
	}

	@Test
	void contextLoads() {

		ShoppingCartDTO shoppingCartDTO = new ShoppingCartDTO("", Arrays.asList(new ShoppingCartDTO.ShoppingCartRow("", "10", "2")));
		HttpEntity<ShoppingCartDTO> request = new HttpEntity<>(shoppingCartDTO);

		// When
		ResponseEntity<String> result = this.restTemplate.postForEntity(uri, request, String.class);

		assertEquals(200, result.getStatusCodeValue());
	}

	@Test
	void add_items_to_shopping_cart() {

		//ShoppingCartDTO shoppingCartDTO = shoppingCartService.addItemToShoppingCart();
		//assertThat(shoppingCartDTO, is(notNullValue(ShoppingCartDTO.class)));
	}
}
