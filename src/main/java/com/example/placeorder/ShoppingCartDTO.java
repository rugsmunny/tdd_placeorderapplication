package com.example.placeorder;

import lombok.Value;

import java.util.List;


@Value
public class ShoppingCartDTO {
    String customerNumber;
    List<ShoppingCartRow> rows;

    @Value
    static
    class ShoppingCartRow {
        String item;
        String price;
        String quantity;
    }
}

