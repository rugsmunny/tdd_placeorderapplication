package com.example.placeorder;


import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
@AllArgsConstructor
public class ShoppingCartController {

    ShoppingCartService shoppingCartService;

    @PostMapping("/createOrderFromShoppingCart")
    public String createOrderFromShoppingCart(@RequestBody ShoppingCartDTO shoppingCartDTO) {
       Order order =  shoppingCartService.placeOrderFromShoppingCart(shoppingCartDTO.getCustomerNumber(), shoppingCartDTO.getRows());
       return order.getOrderNumber();
    }
}
