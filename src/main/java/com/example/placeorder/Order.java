package com.example.placeorder;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class Order {
    String orderNumber;
    String customerNumber;
    List<OrderRow> orderRows;
}
