package com.example.placeorder;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class OrderRow {
    String itemNumber;
    double price;
    double quantity;

}
