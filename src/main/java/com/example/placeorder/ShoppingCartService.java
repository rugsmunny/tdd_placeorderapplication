package com.example.placeorder;

import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ShoppingCartService {

       public Order placeOrderFromShoppingCart(String customerNumber, List<ShoppingCartDTO.ShoppingCartRow> rows) {

        List<OrderRow> orderRows = rows.stream()
                .map(shoppingCartRow -> new OrderRow(shoppingCartRow.getItem(), Double.parseDouble(shoppingCartRow.getPrice()), Double.parseDouble(shoppingCartRow.getQuantity())))
                .collect(Collectors.toList());
        Order order = new Order(UUID.randomUUID().toString(), customerNumber, orderRows);
        return order;
    }
}
